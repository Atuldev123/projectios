//
//  HomeViewController.swift
//  GPower
//
//  Created by A Care Indore on 16/09/21.
//

import UIKit
import RxSwift


class HomeViewController: UIViewController {
    
    var username : String?
    var path: UIBezierPath!
    
    @IBOutlet weak var lblUserName: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let name = username else {
            return
        }
        lblUserName.text = "Hello  \(name)"
      }

}
