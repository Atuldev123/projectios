//
//  ViewController.swift
//  GPower
//
//  Created by A Care Indore on 16/09/21.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate{
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var lblAlertuser: UILabel!
    @IBOutlet weak var lblAlrtpassword: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.delegate = self
        password.delegate = self
        // Do any additional setup after loading the view.
        loginButton.backgroundColor = .lightGray
        loginButton.isUserInteractionEnabled = false
        loginButton.layer.cornerRadius = 25
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.black.cgColor
    }

    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard let name = userName.text , let pass = password.text else {return}
        if name.count > 0{
            if isUserNameValid(name: name) == false {
                lblAlertuser.text = "Enter proper user name"
            }else{
                lblAlertuser.text = ""
            }
        }else{
            lblAlertuser.text = ""
        }
        
        if pass.count > 0{
            if isValidPassword(password: pass) == false {
                lblAlrtpassword.text = "Password not meeting the criteria"
            }else{
                lblAlrtpassword.text = ""
            }
        }else{
            lblAlrtpassword.text = ""
        }
        if isUserNameValid(name: name) == true && isValidPassword(password: pass) == true{
            loginButton.backgroundColor = UIColor(red: 7/255, green: 22/255, blue: 51/255, alpha: 1)
            loginButton.isUserInteractionEnabled = true
        }else{
            loginButton.backgroundColor = .lightGray
            loginButton.isUserInteractionEnabled = false
        }

    }
    
   

        @IBAction func loginButton(_ sender: Any) {
            
            guard let name = userName.text  else {return}
        saveLoggedState()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                vc.modalPresentationStyle = .fullScreen
                vc.username = name
                self.present(vc , animated: true, completion: nil)
        }


    //Check username using Regex

      func isValidPassword(password: String) -> Bool {
        
        if password.rangeOfCharacter(from: CharacterSet.whitespaces) == nil{
            let passRegEx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{5,}$"
            let pass = password.trimmingCharacters(in: .whitespacesAndNewlines)
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", passRegEx)
            return passwordTest.evaluate(with: pass)
        }else{
            return false
        }
    }
             
    //Check username using Regex
      func isUserNameValid(name:String) -> Bool {
              let passRegEx = "^[a-zA-Z\\d]{1,}$"
        
              let passwordTest = NSPredicate(format: "SELF MATCHES %@", passRegEx)
              return passwordTest.evaluate(with: name)
      }
    
    // Show Alert message
      
      func showAlert(message:String){
          let alert = UIAlertController(title: "Alert", message:message , preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
          self.present(alert, animated: true)
      
      }
    
    // User login state in the app
    /// call if user logged in
    func saveLoggedState() {
        let def = UserDefaults.standard
        def.set(true, forKey: "is_authenticated") // save true flag to UserDefaults
        def.synchronize()
    }
}

      
     
